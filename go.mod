module bitbucket.org/cpoac/movie-apis-go

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/form3tech-oss/jwt-go v3.2.3+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e
)
