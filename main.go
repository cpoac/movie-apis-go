package main

//"https://bitbucket.org/bakingthecookie/mnc-log-go/src/master/functions.go"
import (
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/cpoac/movie-apis-go/movie"
	"github.com/gorilla/mux"
)

func main() {
	movie.LoadEnv()
	buildRequests()
}
func buildRequests() {
	Router := mux.NewRouter().StrictSlash(true)
	/* Auth Router*/
	Router.HandleFunc("/register", movie.UserReq{}.Register).Methods(http.MethodPost)
	Router.HandleFunc("/login", movie.UserReq{}.Login).Methods(http.MethodPost)

	RouterRestrict := Router.NewRoute().Subrouter()
	RouterRestrict.Use(movie.JwtVerify)
	RouterRestrict.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "movieDBConnection")
	})
	RouterRestrict.HandleFunc("/comments", movie.CommentReq{}.Index).Methods(http.MethodGet)
	RouterRestrict.HandleFunc("/comments/{id}", movie.CommentReq{}.Index).Methods(http.MethodGet)
	RouterRestrict.HandleFunc("/comments/filteredByMovie/{id}", movie.CommentReq{}.FilteredByMovie).Methods(http.MethodGet)
	RouterRestrict.HandleFunc("/comments", movie.CommentReq{}.Store).Methods(http.MethodPost)

	/* Movies RouterRestrict */
	RouterRestrict.HandleFunc("/movies", movie.MovieReq{}.Index).Methods(http.MethodGet)
	RouterRestrict.HandleFunc("/movies/{id}", movie.MovieReq{}.Show).Methods(http.MethodGet)

	httpPort := os.Getenv("HTTP_PORT")
	fmt.Println("puerto", httpPort)
	if httpPort == "" {
		httpPort = "8080"
	}

	fmt.Println(httpPort)

	log.Fatal(http.ListenAndServe(":"+httpPort, Router))
}

var NotImplemented = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Not Implemented"))
})
