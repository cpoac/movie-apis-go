package movie

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type MovieReq struct{}

//map[string]interface{}{"status": false, "message": "Invalid request"}
func (movReq MovieReq) Index(w http.ResponseWriter, r *http.Request) {
	movies, err := getMoviesPopulars()
	if err != nil {

		json.NewEncoder(w).Encode(responseApi{Result: false, Message: err.Error()})
	}
	json.NewEncoder(w).Encode(movies)
}

func (movReq MovieReq) Show(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	movie, _ := getMovie(id)
	json.NewEncoder(w).Encode(movie)
}
