package movie

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type CommentReq struct{}

func (comReq CommentReq) Index(w http.ResponseWriter, r *http.Request) {
	comments, err := getAllComments()
	fmt.Println("error", err)
	fmt.Println(comments)

	for i, item := range comments {
		fmt.Println(i, item)
	}
	json.NewEncoder(w).Encode(comments)
}

func (comReq CommentReq) FilteredByMovie(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	comments, err := getCommentsFilteredByMovie(id)
	fmt.Println("error", err)
	fmt.Println(comments)

	for i, item := range comments {
		fmt.Println(i, item)
	}
	json.NewEncoder(w).Encode(comments)
}

func (comReq CommentReq) Store(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)

	var comment Comment
	json.Unmarshal(reqBody, &comment)

	_, err := insertComment(comment)

	if err != nil {
		json.NewEncoder(w).Encode(responseApi{Result: false, Message: err.Error()})
	}
	json.NewEncoder(w).Encode(responseApi{Result: true, Message: "Almacenado exitosamente"})
}
