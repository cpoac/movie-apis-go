package movie

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func insertComment(comment Comment) (idInserted int64, err error) {
	db, err := GetConnection()
	if err != nil {
		return 0, err
	}

	defer db.Close()

	comment.UserId = 1
	query := fmt.Sprintf("INSERT INTO comments(movie_id, user_id, comment, created_at, updated_at) VALUES('%v', '%v', '%v', '%v', '%v')", comment.MovieId, comment.UserId, comment.Comment, time.Now().Format("2006-01-02 00:00:00"), time.Now().Format("2006-01-02 00:00:00"))
	insertQuery, err := db.Exec(query)
	if err != nil {
		return 0, err
	}

	commentId, err := insertQuery.LastInsertId()
	if err != nil {
		return 0, err
	}

	return commentId, nil
}

func getAllComments() (listComments []Comment, err error) {
	query := fmt.Sprintf("SELECT * FROM comments")
	return commonQuery(query)
}

func getComment(movieId int) (comment Comment, err error) {
	query := fmt.Sprintf("SELECT * FROM comments WHERE id = '%v' LIMIT 1", movieId)
	comments, err := commonQuery(query)

	if err != nil {
		return comments[0], err
	}
	return comments[0], nil
}

func getCommentsFilteredByMovie(movieId int) (listComments []Comment, err error) {
	query := fmt.Sprintf("SELECT * FROM comments WHERE movie_id = '%v'", movieId)
	return commonQuery(query)
}

func commonQuery(query string) (listComments []Comment, err error) {
	db, err := GetConnection()
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	comments := []Comment{}

	for rows.Next() {
		var comment Comment

		err = rows.Scan(&comment.Id, &comment.MovieId, &comment.UserId, &comment.Comment, &comment.CreatedAt, &comment.UpdatedAt)
		if err != nil {
			return nil, err
		}
		comments = append(comments, comment)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return comments, nil
}
