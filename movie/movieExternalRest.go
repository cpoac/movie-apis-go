package movie

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

const MOVIE_URL string = "http://api.themoviedb.org"
const MOVIEDB_API_KEY string = "0b5c6b902490db711ef7956d6be7f418"

func getMoviesPopulars() (movies []Movie, err error) {
	resp, err := http.Get(baseUrl("3/movie/popular"))
	if err != nil {
		log.Fatal(err)
	}
	responseData, err := ioutil.ReadAll(resp.Body)
	var responseObject HeaderMovie

	json.Unmarshal(responseData, &responseObject)
	return responseObject.Results, nil
}

func getMovie(movieId int) (movie Movie, err error) {
	partUrl := "3/movie/" + strconv.Itoa(movieId)

	resp, err := http.Get(baseUrl(partUrl))
	if err != nil {
		log.Fatal(err)
	}
	responseData, err := ioutil.ReadAll(resp.Body)
	var responseObject Movie
	json.Unmarshal(responseData, &responseObject)
	return responseObject, nil
}

func baseUrl(partUri string) string {
	req, err := http.NewRequest("GET", MOVIE_URL+"/"+partUri, nil)
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}
	bodyUrl := req.URL.Query()
	bodyUrl.Add("api_key", MOVIEDB_API_KEY)
	bodyUrl.Add("language", "es-ES")
	req.URL.RawQuery = bodyUrl.Encode()
	fmt.Println(req.URL.String())
	return req.URL.String()
}

func verifyIfExistsMovie() {
	/*
	   $movie = self::getMovie($id);
	          return !empty($movie['id']) ? true : false;
	*/
}
