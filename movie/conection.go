package movie

import (
	"database/sql"
	"fmt"
	"os"
)

func GetConnection() (db *sql.DB, err error) {
	USER := os.Getenv("DB_USER")
	PASS := os.Getenv("DB_PASSWORD")
	HOST := os.Getenv("DB_HOST")
	DBNAME := os.Getenv("DB_NAME")

	connectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s", USER, PASS, HOST, DBNAME)

	println(connectionString)
	// Create connection pool
	db, err = sql.Open("mysql", connectionString)
	if err != nil {
		return nil, err
	}
	return db, nil
}
