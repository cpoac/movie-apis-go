package movie

import "github.com/form3tech-oss/jwt-go"

type Comment struct {
	Id        int    `json:"Id"`
	MovieId   int    `json:"MovieId"`
	UserId    int    `json:"UserId"`
	Comment   string `json:"Comment"`
	CreatedAt string `json:"CreatedAt"`
	UpdatedAt string `json:"UpdatedAt"`
}

type User struct {
	Id              int    `json:"Id"`
	Name            string `json:"Name"`
	Email           string `json:"Email"`
	EmailVerifiedAt string `json:"EmailVerifiedAt"`
	Password        string `json:"Password"`
	RememberToken   string `json:"RememberToken"`
	CreatedAt       string `json:"CreatedAt"`
	UpdatedAt       string `json:"UpdatedAt"`
	Token           string `json:"Token"`
}

type JwtUser struct {
	UserId         int
	Name           string
	Email          string
	StandardClaims jwt.Claims
}

type responseApi struct {
	Result  bool     `json:"Result"`
	Message string   `json:"Message"`
	Data    struct{} `json:"Data"`
}

type HeaderMovie struct {
	Page         int64   `json:"Page"`
	TotalPages   int64   `json:"TotalPages"`
	TotalResults int64   `json:"TotalResults"`
	Results      []Movie `json:"Results"`
}

type Movie struct {
	Adult            bool    `json:"Adult"`
	BackdropPath     string  `json:"BackdropPath"`
	GenreIDS         []int64 `json:"GenreIDS"`
	ID               int64   `json:"ID"`
	OriginalLanguage string  `json:"OriginalLanguage"`
	OriginalTitle    string  `json:"OriginalTitle"`
	Overview         string  `json:"Overview"`
	Popularity       float64 `json:"Popularity"`
	PosterPath       string  `json:"PosterPath"`
	ReleaseDate      string  `json:"ReleaseDate"`
	Title            string  `json:"Title"`
	Video            bool    `json:"Video"`
	VoteAverage      float64 `json:"VoteAverage"`
	VoteCount        int64   `json:"VoteCount"`
}
