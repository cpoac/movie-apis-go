package movie

import (
	"fmt"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(user User) (idInserted int64, err error) {

	db, err := GetConnection()
	if err != nil {
		return 0, err
	}

	defer db.Close()

	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

	if err != nil {
		return 0, err
	}
	//Save user password
	user.Password = string(pass)
	user.CreatedAt = time.Now().Format("2006-01-02 00:00:00")
	user.UpdatedAt = time.Now().Format("2006-01-02 00:00:00")
	user.EmailVerifiedAt = time.Now().Format("2006-01-02 00:00:00")

	query := fmt.Sprintf("INSERT INTO users(name, email, email_verified_at, password, created_at, updated_at) VALUES('%v', '%v', '%v', '%v', '%v', '%v')", user.Name, user.Email, user.EmailVerifiedAt, user.Password, user.CreatedAt, user.UpdatedAt)
	insertQuery, err := db.Exec(query)
	if err != nil {
		return 0, err
	}

	commentId, err := insertQuery.LastInsertId()
	if err != nil {
		return 0, err
	}

	return commentId, nil
}

func Login(username string, password string) (resp User, err error) {

	db, err := GetConnection()
	if err != nil {
		return User{}, err
	}

	defer db.Close()

	query := fmt.Sprintf("SELECT id, name, email, password, created_at, updated_at FROM users WHERE email = '%v'  LIMIT 1", username)

	rows, err := db.Query(query)
	if err != nil {
		return User{}, err
	}
	var user User

	for rows.Next() {

		err = rows.Scan(&user.Id, &user.Name, &user.Email, &user.Password, &user.CreatedAt, &user.UpdatedAt)
		if err != nil {
			return User{}, err
		}
	}

	err = rows.Err()
	if err != nil {
		return User{}, err
	}

	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if errf != nil || errf == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		return User{}, errf
		//return resp
	}

	expiresAt := time.Now().Add(time.Minute * 100000).Unix()

	tk := &jwt.StandardClaims{
		Id:        strconv.Itoa(user.Id),
		ExpiresAt: expiresAt,
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, error := token.SignedString([]byte("secret"))
	if error != nil {
		fmt.Println(error)
	}
	user.Token = tokenString

	return user, nil

}
