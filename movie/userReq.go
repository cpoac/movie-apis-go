package movie

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type UserReq struct{}

func (usReq UserReq) Register(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var user User
	json.Unmarshal(reqBody, &user)

	_, err := CreateUser(user)

	if err != nil {
		json.NewEncoder(w).Encode(responseApi{Result: false, Message: err.Error()})
	}
	json.NewEncoder(w).Encode(responseApi{Result: true, Message: "Usuario generado exitosamente"})

}

func (usReq UserReq) Login(w http.ResponseWriter, r *http.Request) {

	reqBody, _ := ioutil.ReadAll(r.Body)
	var user User
	json.Unmarshal(reqBody, &user)

	user, err := Login(user.Email, user.Password)

	if err != nil {
		json.NewEncoder(w).Encode(responseApi{Result: false, Message: err.Error()})
	} else {
		json.NewEncoder(w).Encode(user)
	}

}
